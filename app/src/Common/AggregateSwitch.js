import React from "react";
import Switch from "react-switch";
import PropTypes from "prop-types";

AggregateSwitch.propTypes = () => { 
	return { 
		setAggregate: PropTypes.func,
		aggregate: PropTypes.bool,
		switchText: PropTypes.string
	};
};

export default function AggregateSwitch(props) {
 
	return (
		<label>
			<p>{props.switchText}</p>
			<Switch onColor="#35D07F" onChange={(checked, event, id) => props.setAggregate(checked)} checked={props.aggregate} />
		</label>
	);
}