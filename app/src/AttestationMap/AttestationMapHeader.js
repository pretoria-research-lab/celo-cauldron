import React, {useState} from "react";
import PropTypes from "prop-types";
import securityCheckLogo from "../assets/11-Security Check (dark-bg).png";
import Slider from "rc-slider";
import "rc-slider/assets/index.css";
import RefreshSwitch from "../Common/RefreshSwitch";
import OnlyFavouritesSwitch from "../Common/OnlyFavouritesSwitch";
// import AggregateSwitch from "../Common/AggregateSwitch";

AttestationMapHeader.propTypes = () => { 
	return { 
		network: PropTypes.any,
		blockNumber: PropTypes.any,
		
		setAutoRefresh: PropTypes.func,		
		autoRefresh: PropTypes.bool,
		
		setOnlyFavourites: PropTypes.func,
		onlyFavourites: PropTypes.bool,

		scale: PropTypes.any,
		jumpToEpoch: PropTypes.func
	}; 
};	

const getIndexFromValue = (value) => {
	var index = 0;
	var intValue = parseInt(value); 
	switch (intValue) {
	case 10:
		index = 0;
		break;
	case 50:
		index = 1;
		break;
	case 100:
		index = 2;
		break;
	case 250:
		index = 3;
		break;
	case 300:
		index = 4;
		break;
	default:
		index = 0;
	}
	return index;
};

const marks = 
{ 
	0:	{	style: {
		color: "var(--celo-green)",},
	label: <strong>10</strong>
	},
	1:	{	style: {
		color: "var(--celo-green)",},
	label: <strong>50</strong>
	},
	2:	{	style: {
		color: "var(--celo-green)",},
	label: <strong>100</strong>
	},
	3:	{	style: {
		color: "var(--celo-green)",},
	label: <strong>250</strong>
	},
	4:	{	style: {
		color: "var(--celo-green)",},
	label: <strong>300</strong>
	},
};

export default function AttestationMapHeader(props) {

	const title = props.network.toLowerCase() === "mainnet" ? "" : (props.network + " ");
	const index = getIndexFromValue(props.scale);
	const [atEpoch, setAtEpoch] = useState(0);

	return (
		<>
			<div className="row headerRow centered">
				<div className="col-sm-5">
					<div className="row page-header">
						<img className="celoIcons" src={securityCheckLogo} alt="Security check icon" /> 
						<h2 className="mt">{"Celo " + title + "Attestations Map"}</h2>
					</div>
				</div>
				<div className="col-sm-7">
					<div className="row controls">
						<div className="col-sm-3">			
							<table className="table table-dark table-bordered">
								<tbody>
									<tr><td>Block</td><td>{props.blockNumber ? props.blockNumber : "loading..."}</td></tr>
									<tr><td>Epoch</td><td>{props.epochNumber ? props.epochNumber : "loading..."}</td></tr>
								</tbody>
							</table>
						</div>				
						<div className="col-sm-2">			
							<RefreshSwitch switchText={"Auto-refresh"} setStayAtHead={props.setAutoRefresh} stayAtHead={props.autoRefresh}/>
						</div>
						<div className="col-sm-2">			
							<OnlyFavouritesSwitch setOnlyFavourites={props.setOnlyFavourites} onlyFavourites={props.onlyFavourites}/>
						</div>	
						{/* <div className="col-sm-2">			
							<AggregateSwitch switchText={"Since Genesis"} setAggregate={props.setAggregate} aggregate={props.aggregate}/>
						</div>	 */}
						<div className="col-sm-2">					
							<div className="column">
								<input defaultValue={0} onChange={(event, type)=>{
									event.preventDefault();
									event.persist();
									setAtEpoch(event.target.value);
								}} name="jumpToBlock" className="form-control" id="jumpToBlock" type="text"></input>
								<button onClick={()=> props.jumpToEpoch(atEpoch)} className="btn-secondary">To Epoch</button>
							</div>
						</div>			
						<div className="col-sm-3">
							<p>View Size</p>
							<Slider min={0} max={4} step={1} defaultValue={index} marks={marks} onAfterChange={(value) => props.changeMapScale(value)}/>
						</div>
					</div>					
				</div>
			</div>	
		</> 
	);
}