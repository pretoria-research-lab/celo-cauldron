import React from "react";
import PropTypes from "prop-types";
import Pagination from "react-bootstrap/Pagination";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap";

EpochPaginator.propTypes = () => { 
	return { 
		changeToEpoch: PropTypes.func,
		pageList: PropTypes.any,
		changeToFirstPage: PropTypes.func,
		changeToLastPage: PropTypes.func
	};
};

const nextPage = (pageList, changeToEpoch) => {
	pageList.forEach((page,i) => {
		if(page.active && i < pageList.length-1){
			changeToEpoch(pageList[i+1].atEpoch);
		}
	});
};

const previousPage = (pageList, changeToEpoch) => {
	pageList.forEach((page,i) => {
		if(page.active && i > 0){
			changeToEpoch(pageList[i-1].atEpoch);
		}
	});
};

export default function EpochPaginator(props) {

	const { pageList, changeToEpoch, changeToLastPage, changeToFirstPage } = props;

	return (
		<div className="row">
			<div className="column" id="react-paginate">
				<Pagination>
					<Pagination.First onClick={() => changeToFirstPage()}/>
					<Pagination.Prev onClick={() => previousPage(pageList, changeToEpoch)}/>
					{pageList.map((page, i) => 
						page.active ? <Pagination.Item key={i} active onClick={() => changeToEpoch(page.atEpoch)}>{page.atEpoch}</Pagination.Item> 
							: <Pagination.Item key={i} onClick={() => changeToEpoch(page.atEpoch)}>{page.atEpoch}</Pagination.Item>
					)}
					<Pagination.Next onClick={() => nextPage(pageList, changeToEpoch)}/>
					<Pagination.Last onClick={() => changeToLastPage()}/>
				</Pagination>
			</div> 
		</div>
	);
}